//
//  Task.swift
//  TodoList
//
//  Created by Yuriy Pertsev on 29.04.2020.
//  Copyright © 2020 Archer. All rights reserved.
//

import Foundation

struct Task {
    var title: String?
    var description: String?
}
