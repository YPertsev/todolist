//
//  ViewController.swift
//  TodoList
//
//  Created by Yuriy Pertsev on 29.04.2020.
//  Copyright © 2020 Archer. All rights reserved.
//

import UIKit

let taskCellIdentifier = "TaskCellIdentifier"

class ViewController: UIViewController {
    
    //the link to tableView
    @IBOutlet weak var taskTableView: UITableView!
    
    //object which received from details screen
    var task: Task? {
            didSet {
                if let task = task {
                    tasks.append(task)
                }
            }
    }
    // the list of tasks that are shown in the table
    var tasks: [Task] = [] {
        didSet {
            taskTableView.reloadData()
        }
    }
    // index of selected row
    var selectedRow: Int?
    
    // method for set settings of the screen
    override func viewDidLoad() {
        super.viewDidLoad()
        self.taskTableView.register(UITableViewCell.self, forCellReuseIdentifier: taskCellIdentifier)
    }
    
    // method for show details screen
    @IBAction func pressedAddTask(_ sender: Any) {
        performSegue(withIdentifier: "goToDetails", sender: self)
    }
    
    // the method is executed when we close the screen with the details
    @IBAction func unwindToThisView(sender: UIStoryboardSegue) {
        guard let tasksVC = sender.source as? DetailsViewController else { return }
            if let task = tasksVC.task {
                self.task = task
            }
        
        taskTableView.reloadData()
    }
    
    // method for set settings in details screen when we open it
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToDetails" {
            let detailsVC = segue.destination as! DetailsViewController
            if !tasks.isEmpty, let selectedRow = selectedRow {
                detailsVC.task = tasks[selectedRow]
                self.selectedRow = nil
            }
        }
    }
        
}

// this extension of our main screen for working with tableView
extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    // this method return count of rows in tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    // this method create row with information
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: taskCellIdentifier) as UITableViewCell?
        
        if !tasks.isEmpty {
            let task = tasks[indexPath.row]
            cell?.textLabel?.text = task.title
            cell?.detailTextLabel?.text = task.description
        }
        return cell!
    }
    
    // this method is executed when user pressed on row of the tableView
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.selectedRow = indexPath.row
        performSegue(withIdentifier: "goToDetails", sender: self)
    }
    
}
