//
//  DetailsViewController.swift
//  TodoList
//
//  Created by Yuriy Pertsev on 29.04.2020.
//  Copyright © 2020 Archer. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    // links to the items on screen
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    //object which we show on screen
    var task: Task?
    
    // method for set settings of the screen
    override func viewDidLoad() {
        super.viewDidLoad()
        if task == nil {
            task = Task(title: "", description: "")
        }
        descriptionTextView.layer.borderWidth = 1
        descriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    // method for set settings when screen start showing
    override func viewWillAppear(_ animated: Bool) {
             titleTextField.text = task?.title
             descriptionTextView.text = task?.description
    }
    
    // method for set settings in main screen when we return to it
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToList" {
            task?.title = titleTextField.text
            task?.description = descriptionTextView.text
        }
    }
}
